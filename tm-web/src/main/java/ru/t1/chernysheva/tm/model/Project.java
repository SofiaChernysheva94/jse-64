package ru.t1.chernysheva.tm.model;

import ru.t1.chernysheva.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractModel {

    private String name;

    private String description;

    private Status status = Status.NOT_STARTED;

    private Date dateFinish;

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(Date dateFinish) {
        this.dateFinish = dateFinish;
    }

}
