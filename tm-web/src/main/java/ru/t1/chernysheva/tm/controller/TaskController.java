package ru.t1.chernysheva.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.repository.TaskRepository;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    private TaskRepository taskRepository;

    @GetMapping("")
    public ModelAndView index() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-list");
        modelAndView.addObject("tasks", taskRepository.findAll());
        modelAndView.addObject("projectRepository", taskRepository);
        return new ModelAndView("task-list", "tasks", taskRepository.findAll());
    }


    @GetMapping("/create")
    public String create() {
        taskRepository.save(new Task("Task name"));
        return "redirect:/Tasks";
    }

    @GetMapping("/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        taskRepository.deleteById(id);
        return "redirect:/tasks";
    }

    @GetMapping("/edit/{id}")
    public String edit(
            @PathVariable("task") Task task
    ) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @PostMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final  Task task = taskRepository.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("Task", task);
        modelAndView.addObject("statutes", Status.values());
        return  modelAndView;
    }
    
}
