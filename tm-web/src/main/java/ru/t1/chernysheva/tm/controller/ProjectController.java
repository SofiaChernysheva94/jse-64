package ru.t1.chernysheva.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.repository.ProjectRepository;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;

    @GetMapping("")
    public ModelAndView index() {
        return new ModelAndView("project-list", "projects", projectRepository.findAll());
    }

    @GetMapping("/create")
    public String create() {
        projectRepository.save(new Project("Project name"));
        return "redirect:/projects";
    }

    @GetMapping("/delete/{id}")
    public String delete(
            @PathVariable("id") String id
    ) {
        projectRepository.deleteById(id);
        return "redirect:/projects";
    }

    @GetMapping("/edit/{id}")
    public String edit(
            @PathVariable("Project") Project Project
    ) {
        projectRepository.save(Project);
        return "redirect:/projects";
    }

    @PostMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final Project project = projectRepository.findById(id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statutes", Status.values());
        return  modelAndView;
    }

}
