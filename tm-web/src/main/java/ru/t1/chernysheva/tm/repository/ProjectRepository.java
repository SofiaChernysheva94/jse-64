package ru.t1.chernysheva.tm.repository;

import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    private static final  ProjectRepository INSTANCE = new ProjectRepository();

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    private Map<String, Project> Projects = new LinkedHashMap<>();

    {
        add(new Project("ONE"));
        add(new Project("TWO"));
        add(new Project("THREE"));
    }

    public void add(Project Project) {
        Projects.put(Project.getId(), Project);
    }

    public void create() {
        add(new Project("New Project " + System.currentTimeMillis()));
    }

    public void deleteById(final String id) {
        Projects.remove(id);
    }

    public Collection<Project> findAll() {
        return Projects.values();
    }

    public Project findById(final String id) {
        return Projects.get(id);
    }

    public void save(Project Project) {
        Projects.put(Project.getId(), Project);
    }

}
