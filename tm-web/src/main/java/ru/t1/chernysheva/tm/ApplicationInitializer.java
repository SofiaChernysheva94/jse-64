package ru.t1.chernysheva.tm;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.t1.chernysheva.tm.configuration.ApplicationConfiguration;
import ru.t1.chernysheva.tm.configuration.WebAppConfiguration;

public class ApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {
                WebAppConfiguration.class
        };
    }

    @Override
    protected String[] getServletMappings() {
        return new String[] { "/"};
    }

}
